#version 410

uniform float lightIntensity;
uniform bool blinnPhong;
uniform float shininess;
uniform float eta;
uniform sampler2D shadowMap;

in vec4 eyeVector;
in vec4 lightVector;
in vec4 vertColor;
in vec4 vertNormal;
in vec4 lightSpace;

out vec4 color;

vec4 diffuse(float kd){
 return kd * vertColor * max(dot(normalize(vertNormal), lightVector), 0.0) * lightIntensity;
}
vec4 ambient(float  ka){
return ka * vertColor * lightIntensity;
}
float Fresnel(vec4 H , float cosangle ){

  float ci = pow( pow(eta,2) - (1 - pow(cosangle,2)) ,0.5);
  float Fs = pow( (cosangle - ci ) / cosangle + ci , 2);
  float Fp = pow((pow(eta,2)*cosangle - ci ) / (pow(eta,2)*cosangle + ci ),2);
  return (Fs + Fp)/2;

}
float schlick_fresnel(vec4 H, float cosangle){
  float Fo =  pow(  (1-eta)/ (1+eta),2   );
  float base = 1.0 - cosangle;
  return Fo +  (1-Fo)*pow(base,5);
}


float Xfunc(float theta){
  if(theta > 0.0){
    return 3.141592 /2;
  }else{
    return 0.0;
  }
}
float Cs(vec4 H , float cosHL,float alpha){
  float F = Fresnel(H,cosHL);
  float alpha_squared = pow(alpha,2);
  float cosVN = dot(eyeVector,vertNormal);
  float cosLN = dot(lightVector,vertNormal);
  float cosHN = dot(H,vertNormal);

  float tanVN = sqrt(1 - pow(cosVN,2)) / cosVN;
  float tanLN = sqrt(1 - pow(cosLN,2)) / cosLN;
  float tanHN = sqrt(1 - pow(cosHN,2)) / cosHN;

  float Gi = 2 / 1+ sqrt( 1 + alpha_squared + pow( tanLN,2 ));
  float Go = 2 / 1+ sqrt( 1 + alpha_squared + pow( tanVN,2 ));

  float D = (Xfunc(cosHN)/ ( 3.141592 * pow(cosHN,4)))
      * ( alpha_squared / pow ( alpha_squared + pow(tanHN , 2 ),2  ));

 return F*D*Gi*Go / 4*cosVN * cosLN;
}

void main( void )
{

  float ka = 0.33;
  float kd = 0.33;
  vec4 specular = vec4(0.0,0.0,0.0,0.0);
  vec4 ev = normalize (eyeVector);
  vec4 lv = normalize(lightVector);
  vec4 H =  normalize(ev + lv);
  float cosangle = dot(H,lightVector);
  float intensity = max(dot(vertNormal, lightVector),0.0);

  if(blinnPhong){
      if(intensity > 0.0){
          specular =  Fresnel(H,cosangle) * vertColor * pow( max(dot(normalize(vertNormal), H),0.0),shininess) * lightIntensity;
      }
  }else{
      if(shininess != 0){
        if(intensity > 0.0){
          float spec = Cs(H,cosangle,1/(shininess));
          specular = vertColor * spec;
        }
      }
  }

    color = diffuse(kd) + ambient(ka) + specular ;

}
