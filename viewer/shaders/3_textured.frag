#version 410

uniform float lightIntensity;
uniform sampler2D colorTexture;
uniform bool blinnPhong;
uniform float shininess;
uniform float eta;
uniform sampler2D shadowMap;

in vec4 eyeVector;
in vec4 lightVector;
in vec4 vertColor;
in vec4 vertNormal;
in vec2 textCoords;
in vec4 lightSpace;

out vec4 color;

vec4 diffuse(float kd, vec4 textureColor){
 return kd * textureColor * max(dot(vertNormal, lightVector), 0.0) * lightIntensity;
}
vec4 ambient(float  ka, vec4 textureColor){
return ka * textureColor * lightIntensity;
}
float Fresnel(vec4 H , float cosangle ){

  float ci = pow( pow(eta,2) - (1 - pow(cosangle,2)) ,0.5);
  float Fs = pow( (cosangle - ci ) / cosangle + ci , 2);
  float Fp = pow((pow(eta,2)*cosangle - ci ) / (pow(eta,2)*cosangle + ci ),2);
  return (Fs + Fp)/2;

}
float schlick_fresnel(vec4 H, float cosangle){
  float Fo =  pow(  (1-eta)/ (1+eta),2   );
  float base = 1.0 - cosangle;
  return Fo +  (1-Fo)*pow(base,5);
}


float Xfunc(float theta){
  if(theta > 0.0){
    return 3.141592 /2;
  }else{
    return 0.0;
  }
}
float Cs(vec4 H , float cosHL,float alpha){
  float F = Fresnel(H,cosHL);
  float alpha_squared = pow(alpha,2);
  float cosVN = max(dot(eyeVector,vertNormal),0.0);
  float cosLN = max(dot(lightVector,vertNormal),0.0);
  float cosHN = max(dot(H,vertNormal),0.0);

  float tanVN = max(sqrt(1 - pow(cosVN,2)) / cosVN,0.0);
  float tanLN = max(sqrt(1 - pow(cosLN,2)) / cosLN,0.0);
  float tanHN = max(sqrt(1 - pow(cosHN,2)) / cosHN,0.0);

  float Gi = 2 / 1+ sqrt( 1 + alpha_squared + pow( tanLN,2 ));
  float Go = 2 / 1+ sqrt( 1 + alpha_squared + pow( tanVN,2 ));

  float D = (Xfunc(cosHN)/ ( 3.141592 * pow(cosHN,4)))
      * ( alpha_squared / pow ( alpha_squared + pow(tanHN , 2 ),2  ));

 return F*D*Gi*Go / 4*cosVN * cosLN;
}
void main( void )
{

float ka = 0.33;
float kd = 0.33;
vec4 specular = vec4(0.0,0.0,0.0,0.0);
vec4 H =  normalize(eyeVector + lightVector);
float cosangle = max(dot(H,lightVector),0.0);
float intensity = max(dot(vertNormal, lightVector),0.0);
vec4 texturecolor = texture2D(colorTexture,textCoords);

  if(blinnPhong){
      specular =    Fresnel(H,cosangle) * texturecolor * pow( max(dot(vertNormal, H),0.0) ,shininess) * lightIntensity;
  }else{
    if(shininess != 0){
      if(intensity > 0.0){
        float spec = Cs(H,cosangle,1/(shininess));
        specular = texturecolor * spec;
      }
    }
  }

    color = diffuse(kd,texturecolor) + ambient(ka,texturecolor) + specular  ;

}
