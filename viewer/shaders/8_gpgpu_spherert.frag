#version 410
#define M_PI 3.14159265358979323846
#define air 1.00
#define glass 2.5


uniform mat4 mat_inverse;
uniform mat4 persp_inverse;
uniform sampler2D envMap;
uniform vec3 center;
uniform float radius;

uniform bool transparent;
uniform float shininess;
uniform float eta;

in vec4 vertColor;
in vec4 vertNormal;
in vec2 textCoords;
in vec4 position;
out vec4 fragColor;


vec4 getColorFromEnvironment(in vec3 r)
{
    float r2 = sqrt(r.x*r.x + r.y*r.y + r.z*r.z);
    float theta = acos(r.z/r2); // [-pi/2;pi/2]
    float phi = atan(r.y,r.x);// [-pi;pi]


    vec2 uv = vec2((phi/ ( M_PI * 2) ) + 0.5,theta / ( M_PI ));

    vec4 env = texture2D(envMap,uv );
    return env;
}

float Fresnel (float n1, float n2,vec3 normal, vec3 incident)
{
  float cosangle = clamp(-1,1,dot(normal,incident));
  float Fs = ((n1 * cosangle) - (n2 * cosangle)) / ((n1 * cosangle) + (n2 * cosangle));
  float Fp = ((n2 * cosangle) - (n1 * cosangle)) / ((n2 * cosangle) + (n1 * cosangle));
  return (Fs * Fs + Fp * Fp)/2;
}

bool raySphereIntersect(in vec3 start, in vec3 direction, out vec3 newPoint) {
  vec3 PC = start - center;
  float b = 2.0 * dot(direction, PC);
  float c = dot(PC, PC) - radius*radius;
  float discriminant = b * b - 4.0 * c;
  if (discriminant < 0.0)
      return false;
  float q;
  if (b < 0.0)
      q = (-b - sqrt(discriminant))/2.0;
  else
      q = (-b + sqrt(discriminant))/2.0;

  float t0 = q;
  float t1 = c / q;

  if (t0 > t1) {
      // if t0 is bigger than t1 swap them around
      float temp = t0;
      t0 = t1;
      t1 = temp;
  }

  if (t1 < 0.0)
      return false;

  if (t0 < 0.0) {
      newPoint = start + direction * t1;
      return true;
  } else {
    newPoint = start + direction * t0;
    return true;
  }


}
struct Ray
{
  vec3 origin;
  vec3 direction;
  vec4 color;
  vec3 reflec;
  vec3 refrac;
  float fresnel;
};

void main(void)
{
    // Step 1: I need pixel coordinates. Division by w?
    vec4 worldPos = position;
    worldPos.z = 1; // near clipping plane
    worldPos = persp_inverse * worldPos;
    worldPos /= worldPos.w;
    worldPos.w = 0;
    worldPos = normalize(worldPos);
    // Step 2: ray direction:
    vec3 u = normalize((mat_inverse * worldPos).xyz);
    vec3 eye = (mat_inverse * vec4(0, 0, 0, 1)).xyz;
    vec3 intersection  = vec3(0.0);

    Ray Rays[5];
    if(raySphereIntersect(eye, u, intersection)){
      vec3 normal = normalize(intersection - center);
      vec3 reflec = normalize(reflect (u, normal));
      vec3 refrac = normalize(refract (u,normal,air/glass));
      Rays[0] =Ray(eye,u,getColorFromEnvironment(reflec),reflec,refrac,  Fresnel(air,eta, normal, u) );
      vec3 bounce = intersection ;//start of refracting ray
      vec3 directin = refrac;// direction of refracting ray
      for(int i=1; i < 5; i++){
        Rays[i] = Ray(bounce,directin,vec4(0.0),vec3(0.0),vec3(0.0),0.0);
        if(raySphereIntersect(bounce + directin *0.01 ,directin,intersection)){
              normal = normalize(  - intersection + center);
              reflec = normalize(reflect (directin, normal));
              refrac =  normalize(refract (directin, normal,glass/air));
              Rays[i].reflec = reflec;
              Rays[i].refrac = refrac;
              Rays[i].fresnel =   1-Fresnel(eta,air, normal, directin);
              Rays[i].color =   getColorFromEnvironment(refrac);
              directin = reflec;
        }
      }
      if(transparent){
          fragColor = vec4(0.0,0.0,0.0,1.0);
         for(int i = 4; i>=0; i--){
           if(i==0){
             fragColor += Rays[i].color * Rays[i].fresnel;
           }else {
             fragColor +=  (1-Rays[i-1].fresnel) *  Rays[i].color * Rays[i].fresnel ;
           }
         }
        //fragColor = (Rays[0].color * Rays[0].fresnel) + ( (1-Rays[0].fresnel) *  Rays[1].color * (Rays[1].fresnel)  ) + ( (1-Rays[1].fresnel) *  Rays[2].color * Rays[2].fresnel)  ;
      }else{
        fragColor = Rays[0].color ;
      }
    }else{
      fragColor = getColorFromEnvironment(u);
    }


    vec4 resultColor = vec4(0,0,0,1);

}
