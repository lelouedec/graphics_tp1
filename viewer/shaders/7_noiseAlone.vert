#version 410

uniform mat4 matrix;
uniform mat4 perspective;
uniform mat3 normalMatrix;
uniform bool noColor;
uniform vec3 lightPosition;
uniform vec4 couleur;

in vec4 vertex;
in vec4 normal;
in vec4 color;

out vec4 eyeVector;
out vec4 lightVector;
out vec4 vertColor;
out vec4 vertNormal;
out vec4 vertPos;

void main( void )
{
    if (noColor) vertColor = couleur;
    else vertColor = couleur;
    vertNormal.xyz = normalize(normalMatrix * normal.xyz);
    vertNormal.w = 0.0;

    lightVector = normalize(vec4(lightPosition,0.0) - vertex );
    eyeVector = normalize(-(matrix * vertex));
    vertPos = vertex;

    gl_Position = perspective * matrix * vertex;
}
