#version 410

uniform float lightIntensity;
uniform bool blinnPhong;
uniform float shininess;
uniform float eta;
uniform sampler2D shadowMap;
uniform vec4 reflectivity;
uniform vec4 edgetint;

in vec4 eyeVector;
in vec4 lightVector;
in vec4 vertColor;
in vec4 vertNormal;
in vec4 lightSpace;

out vec4 color;

vec4 diffuse(float kd,vec4 color){
 return kd * color * max(dot(vertNormal, lightVector), 0.0) * lightIntensity;
}
vec4 ambient(float  ka,vec4 color){
return ka * color * lightIntensity;
}

float n_min(float r){
    return (1-r)/(1+r);
}
float n_max(float r){
    return (1+sqrt(r))/(1-sqrt(r));
}
float get_n(float r,float g){
    return n_min(r)*g + (1-g)*n_max(r);
}
float get_k2(float r, float n){
    float nr = (n+1)*(n+1)*r-(n-1)*(n-1);
    return nr/(1-r);
}
float get_r(float n, float k){
    return ((n-1)*(n-1)+k*k)/((n+1)*(n+1)+k*k);
}
float get_g(float n, float k){
    float r = get_r(n,k);
    return (n_max(r)-n)/(n_max(r)-n_min(r));
}
float fresnel(float r, float g,float theta){
    //clamp parameters
    float r2 = clamp(r,0.0,0.99);
    //compute n and k
    float n = get_n(r2,g);
    float k2 = get_k2(r2,n);

    float c = theta;
    float rs_num = n*n + k2 - 2*n*c + c*c;
    float rs_den = n*n + k2 + 2*n*c + c*c;
    float rs = rs_num/rs_den;

    float rp_num = (n*n + k2)*c*c - 2*n*c + 1;
    float rp_den = (n*n + k2)*c*c + 2*n*c + 1;
    float rp = rp_num/rp_den;

    return 0.5*(rs+rp);
}

void main( void )
{

  float ka = 0.33;
  float kd = 0.33;
  vec4 specular = vec4(0.0,0.0,0.0,0.0);
  vec4 H =  normalize(eyeVector + lightVector);
  float cosangle = dot(H,lightVector);
  float intensity = max(dot(vertNormal, lightVector),0.0);

      if(intensity > 0.0){
          specular = vec4( fresnel(reflectivity.x,edgetint.x,cosangle),  fresnel(reflectivity.y,edgetint.y,cosangle),  fresnel(reflectivity.z,edgetint.z,cosangle), 1.0);
          specular = specular * vertColor * pow( max(dot(vertNormal, H),0.0),shininess) * lightIntensity;
      }


    color = diffuse(kd,edgetint) + ambient(ka,edgetint) + specular ;

}
